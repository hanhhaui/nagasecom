
import React from 'react';
import validate from './validateInfo';
import useForm from './useForm';
import { useState } from 'react';
import { Row , Col, Accordion,Alert,Button } from "react-bootstrap";

const FormSignup = ({ submitForm }) => {
  const { handleChange, handleSubmit, values, errors } = useForm(
    submitForm,
    validate
  );
  const [show, setShow] = useState(true);
  if(show){
    return (
      <div className='all'>
        <form onSubmit={handleSubmit} className='form' noValidate>
          <header>
            <div className='header-logo'>  
            </div>
            {/* <Alert variant="danger" onClose={() => setShow(false)} dismissible>
              <Alert.Heading>Oh snap! You got an error!</Alert.Heading>
              <p>
                Change this and that and try again.
              </p>
            </Alert> */}
          </header>
          <div className='above'></div>
          <section>
              <input type="checkbox" id="show"/>
              <label for="show" class="show-btn">Login</label>
              <input type="checkbox" id="showR"/>
              <label for="showR" class="show-btnR">Register</label>
                <div className='login'>
                  <div className='content-log'>
                  <label for="show" class="close-btn fas fa-times" title="close"></label>
                    <p className='text'>Wellcome back!</p>
                    <p className='text-small'>Enter your NVN code and password to sign in.</p>
                    <div className='user'>
                      <p className='name' >User name *</p>
                      <input
                            className='ip-name' 
                            placeholder='Username' 
                            name='username' 
                            type='text'  
                            value={values.username}
                            onChange={handleChange} />
                      {errors.username && <p className='error-text'>{errors.username}</p>}
                      <p className='pass'>Pass Word *</p>
                      <input 
                              className='ip-pas'
                              placeholder='Password' 
                              name='password' 
                              type='password' 
                              value={values.password}
                              onChange={handleChange} />
                
                      {errors.password && <p className='error-text'>{errors.password}</p>}
                      <div className='accept'>
                        <label class='switch'>
                          <input type='checkbox' />
                          <span class='slider round'></span>
                        </label>
                        <p className='remember'>Remember me</p>
                      </div>
                      <button className='button-login'>Login</button>
                      {/* <Button className='button-login' onClick={() => setShow(true)}>Login</Button> */}
                      <a id='ifor' className='show-btnF' href='#'>Forgot Password?</a>
                    </div>      
                  </div>
              </div>
              <div className='regis'>
                  <div className='content'>
                      <label for="showR" class="close-btn fas fa-times" title="close"></label>
                      <div className='create-pas'>Create Passsword</div>
                      <input type='password' placeholder='Password' className='in-pas'>
                          
                      </input>
                      <input type='password' placeholder='Re-enter Password' className='in-pas'></input>
                      <div className='note'>
                          <div className='text-mus'>Your password must have</div>
                          <ul className='list1'>
                              <li><a href="#">6 or more characters</a></li>
                              <li><a href="#">Upper and lowercase letters</a></li>
                              <li><a href="#">At least 1 number</a></li>
                              <li><a href="#">At least 1 speacial characters</a></li>
                          </ul>
                      </div>
                      <button className='button-save' type='submit'>Save</button>
                  </div>
              </div>
              <div id='rr' className='reset'>
                <div className='content-reset'>
                    <div className='create-pas'>Reset Passsword</div>
                    <p>Email *</p>
                    <input 
                        placeholder='Example@gmail.com' 
                        name='reset-pass' 
                        type='text'  
                    />
                    <div className='buttons'>
                        <button className='button-cancel' type='submit'>Cancel</button>
                        <button className='button-submit' type='submit'>Submit</button>
                    </div>
                </div>
             </div>
           </section>
          <footer></footer>
        </form>
      </div>
    ); 
  }
}
export default FormSignup;