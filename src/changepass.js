import { tab } from "@testing-library/user-event/dist/tab";
import React from "react";
import { Row , Col, Accordion,Button,Offcanvas,Spinner,Form,FormCheck,FormGroup } from "react-bootstrap";
import { useState } from "react";
export default function Changepass() {
    
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    document.getElementById("button-1").onclick = function () {
        document.getElementById("info1").style.display = 'block';
        document.getElementById("info2").style.display = 'none';
        document.getElementById("info3").style.display = 'none';
        document.getElementById("info4").style.display = 'none';
    };
    document.getElementById("button-2").onclick = function () {
        document.getElementById("info2").style.display = 'block';
        document.getElementById("info1").style.display = 'none';
        document.getElementById("info3").style.display = 'none';
        document.getElementById("info4").style.display = 'none';
    };
    document.getElementById("button-3").onclick = function () {
        document.getElementById("info3").style.display = 'block';
        document.getElementById("info1").style.display = 'none';
        document.getElementById("info2").style.display = 'none';
        document.getElementById("info4").style.display = 'none';
    };
    document.getElementById("aa").onclick = function () {
        document.getElementById("info4").style.display ='block';
        document.getElementById("info3").style.display = 'none';
        document.getElementById("info1").style.display = 'none';
        document.getElementById("info2").style.display = 'none';
        document.getElementById("an").style.display = 'none';
    };
    document.getElementById("ab").onclick = function () {
        document.getElementById("an").style.display ='block';
        document.getElementById("info4").style.display ='none';
        document.getElementById("info3").style.display = 'none';
        document.getElementById("info1").style.display = 'none';
        document.getElementById("info2").style.display = 'none';
    };
    return(
        <div className='change'>
            <div className='menu-bar'>
                <div className='logo-small'></div>
                    <div className='menu'>
                        <div className='dashboard'>
                            <div className='dash-img'></div>
                        </div>
                        <div className='main-menu'>
                            <Accordion defaultActiveKey="0" flush>
                                <Accordion.Item eventKey="0">
                                    <Accordion.Header><i class="icon fas fa-users"></i>Account Management<i class="fas fa-chevron-down icn"></i></Accordion.Header>
                                        <Accordion.Body>
                                            <li><a href="#info4" id="aa">User Management</a></li>
                                            <li>Role Management</li>
                                            <li>Department Management</li>
                                            <li>Activity log</li>
                                            <li>Notification Management</li>
                                        </Accordion.Body>
                                </Accordion.Item>
                                <Accordion.Item eventKey="1">
                                    <Accordion.Header><i class="icon fas fa-calendar-alt"></i>PO Management<i class="fas fa-chevron-down icn"></i></Accordion.Header>
                                        <Accordion.Body>
                                            <li><a href="#an" id="ab">Stock control</a></li>
                                            <li>New Order Plan List</li>
                                            <li>Pending Confirmation</li>
                                            <li>Pending Approval</li>
                                            <li>Genaral Order Plan List</li>
                                            <li>Cargo Rotation</li>
                                        </Accordion.Body>
                                </Accordion.Item>
                                <Accordion.Item eventKey="2">
                                    <Accordion.Header><i class="icon fas fa-shopping-cart"></i>Oder Plan Management<i class="fas fa-chevron-down icn"></i></Accordion.Header>
                                        <Accordion.Body>
                                            <li>Stock Control</li>
                                            <li>New Order Plan List</li>
                                            <li>Pending Confirmation</li>
                                            <li>Pending Approval</li>
                                            <li>Genaral Order Plan List</li>
                                            <li>Cargo Rotation</li>
                                        </Accordion.Body>
                                </Accordion.Item>
                                <Accordion.Item eventKey="3">
                                    <Accordion.Header><i class="icon far fa-file-alt"></i>Trading Contract Management<i class="fas fa-chevron-down icn"></i></Accordion.Header>
                                        <Accordion.Body>
                                            <li>Stock Control</li>
                                            <li>New Order Plan List</li>
                                            <li>Pending Confirmation</li>
                                            <li>Pending Approval</li>
                                            <li>Genaral Order Plan List</li>
                                            <li>Cargo Rotation</li>
                                        </Accordion.Body>
                                </Accordion.Item>
                                <Accordion.Item eventKey="4">
                                    <Accordion.Header><i class="icon fas fa-envelope-open-text"></i>Delivery Order Management<i class="fas fa-chevron-down icn"></i></Accordion.Header>
                                        <Accordion.Body>
                                            <li>Stock Control</li>
                                            <li>New Order Plan List</li>
                                            <li>Pending Confirmation</li>
                                            <li>Pending Approval</li>
                                            <li>Genaral Order Plan List</li>
                                            <li>Cargo Rotation</li>
                                        </Accordion.Body>
                                </Accordion.Item>
                                <Accordion.Item eventKey="5">
                                    <Accordion.Header><i class="icon fas fa-door-open"></i>Master Data<i class="fas fa-chevron-down icn"></i></Accordion.Header>
                                        <Accordion.Body>
                                            <li>Stock Control</li>
                                            <li>New Order Plan List</li>
                                            <li>Pending Confirmation</li>
                                            <li>Pending Approval</li>
                                            <li>Genaral Order Plan List</li>
                                            <li>Cargo Rotation</li>
                                        </Accordion.Body>
                                </Accordion.Item>
                            </Accordion>
                        </div>
                    </div>
                <div className='end'></div>
                </div>
                <div className='content1'>
                    
                    <div className='top-main'>
                        <div>
                            <ul className="menu-left">
                                <li><i class="fas fa-home"></i></li>/
                                <li><a className="aa" href="#">Profile setting</a></li>
                            </ul>
                        </div>
                        <div>
                            <ul className="menu-right">
                                <li><i class="fas fa-search"></i></li>|
                                <li><i class="far fa-bell"></i></li>|
                                <li><i class="far fa-user"></i></li>
                            </ul>
                        </div>
                     </div>
                <div id="an" className='main-container'>
                    <h2  style={{marginLeft:50}}>Profile setting</h2>
                    <button type="submit" id='button-1'>Personal Information </button>
                    <button type="submit" id='button-2'>Password</button>
                    <button type="submit" id='button-3'>Notification Setting</button>
                </div>
                <div className='info1-container' id='info1'>
                    <br/>
                    <p>NVN Code</p>
                    
                    <p>Full name</p>
                    
                    <p>Department</p>
                    
                    <p>Gender</p>
                    
                    <p>Phone number</p>
                   
                    <p>Email nagese.com</p>
                    <input className='in-vn' type='text' placeholder="@nagase.com.vn"></input>
                    <p>Email nagase.co.jp</p>
                    <input className='in-jp' type='text' placeholder="@nagase.co.jp"></input><br/>
                    <br/>
                    <button className='button-update' type='submit'>Update</button>
                </div>
                <div className='info2-container' id='info2'>
                    <br/>
                    <p className='current'>Current Password *</p>
                    <input  type='password' placeholder='Password' className='in-pass'/>
                    <p className='current'>New password  *</p>
                    <input 
                        name="re-pass"
                        // value={values.re-pass}
                        // onChange={handleChange}     
                        type='password' 
                        placeholder='Password' 
                        className='re-pass'/>
                    <p className='current'>Re-enter new password  *</p>
                    <input 
                        name="ree-pass"
                        // value={values.ree-pass}
                        // onChange={handleChange}
                        type='password' 
                        placeholder='Re-enter Password' 
                        className='in-pass'/>
                    {/* {errors.ree-pass && <p className='error-text'>{errors.ree-pass}</p>} */}
                    <div className='note-change'>
                        <div className='text-mus'>Your password must have</div>
                        <ul className='list-change'>
                            <li><a href="#">6 or more characters</a></li>
                            <li><a href="#">Upper and lowercase letters</a></li>
                            <li><a href="#">At least 1 number</a></li>
                            <li><a href="#">At least 1 speacial characters</a></li>
                        </ul>
                    </div>
                    <button className='button-save' type='submit'>Change Password</button>
                </div>
                
                <div className='info3-container' id='info3'> 
                     <br/>
                     <br/>
                    <table>
                        <tr>
                            <td className="d1">Action</td>
                            <td className="d11">Notification Status</td>
                        </tr>
                        <tr>
                            <td>Product Leadtime was approved</td>
                            <td className="td">
                                <label class="switch">
                                    <input type="checkbox" />
                                    <span class="slider round"></span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>Product Leadtime was rejected</td>
                            <td className="td">
                                 <label class="switch">
                                    <input type="checkbox" />
                                    <span class="slider round"></span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>Selling and Buying Price was approved</td>
                            <td className="td">
                                <label class="switch">
                                    <input type="checkbox"/>
                                    <span class="slider round"></span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>Selling and Buying Price was rejected</td>
                            <td className="td">
                                <label class="switch">
                                    <input type="checkbox" />
                                    <span class="slider round"></span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>Oder plan was confirmed by Sale</td>
                            <td className="td">
                                <label class="switch">
                                    <input type="checkbox" />
                                    <span class="slider round"></span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>Oder plan was rejected by Sale</td>
                            <td className="td">
                                <label class="switch">
                                    <input type="checkbox" />
                                    <span class="slider round"></span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>Oder plan was confirmed  by CS Leader</td>
                            <td className="td">
                                <label class="switch">
                                    <input type="checkbox" />
                                    <span class="slider round"></span>
                                </label>
                            </td>
                        </tr>
                    </table>
                </div>
                <div className='infor4' id="info4">
                         <h2>User management</h2>
                         <br></br>
                         <Button style={{backgroundColor:"#4787A2"}} onClick={handleShow}>
                         <i class="fas fa-plus-circle"></i> Create new user
                        </Button>
                        <Offcanvas placement='end' show={show} onHide={handleClose}>
                            <Offcanvas.Header closeButton>
                                <Offcanvas.Title>Create new user</Offcanvas.Title> 
                                <Spinner animation="border" variant="success" />                         
                            </Offcanvas.Header>
                            <Offcanvas.Body>
                                <hr/>
                                <p className="pd">NVN code *</p>
                                <input className="ind" type='text' placeholder=""></input>
                                <p className="pd">Full name * *</p>
                                <input className="ind" type='text' placeholder=""></input>
                                <p className="pd">Email *</p>
                                <input className="ind" type='text' placeholder=""></input>
                                <p className="pd">Department *</p>
                                <select className="department" id="depart">
                                    <option value="D9">D9</option>
                                    <option value="D6">D6</option>
                                    <option value="D5">D5</option>
                                    <option value="D4">D4</option>
                                </select>
                                <p className="pd">Role *</p>
                                <input className="ind" type='text'/>
                                <div className="check">
                                    <Row className="r1">
                                        <input className="insea" type='text' placeholder="Search"></input>
                                    </Row>
                                    <Row>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="defaultUnchecked" checked/>
                                            <label class="custom-control-label" for="defaultUnchecked">CS leader</label>
                                        </div>
                                    </Row>
                                    <Row>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="defaultUnchecked"/>
                                            <label class="custom-control-label" for="defaultUnchecked">CS</label>
                                        </div>
                                    </Row>
                                    <Row>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="defaultUnchecked"/>
                                            <label class="custom-control-label" for="defaultUnchecked">Sales</label>
                                        </div>
                                    </Row>
                                </div>
                                <Button className="but" variant="light">Cancel</Button> 
                                <Button className="but" variant="success">Create</Button>{' '}
                            </Offcanvas.Body>
                        </Offcanvas>
                </div>
            </div>
        </div>
    );
    
};
